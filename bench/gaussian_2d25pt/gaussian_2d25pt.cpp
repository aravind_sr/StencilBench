/*
 * gaussian_2d25pt.cpp
 * TODO UPDATE header and license
 *
 * Distributed under terms of the GNU LGPL3 license.
 */

#include <iostream>

#include "gaussian_2d25pt.hpp"
#include <stencilbench.hpp>

DTYPE A[N][N];
DTYPE B[N][N];

static void init_arrays()
{
    for (int i = 0; i < N; ++i)
    {
        for (int j = 0; j < N; ++j)
        {
		A[i][j] = (i + j) / N;
        }
    }
}

static void print_arrays()
{
    for (int i = 0; i < N; ++i)
    {
        for (int j = 0; j < N; ++j)
        {
		std::cout << A[i][j] << " " << B[i][j] << " ";
        }
        std::cout << "\n\n";
    }
}

static void gaussian_2d25pt_kernel()
{
    for (int t = 0; t < TSTEPS; ++t)
    {
        for( int i = 2 ; i < N - 2 ; i++ )
            for( int j = 2 ; j < N - 2 ; j++ )
                B[i][j] = (          A[i - 2][j - 2] +
                                     A[i + 2][j - 2] +
                                     A[i - 2][j + 2] +
                                     A[i + 2][j + 2] +
                                4 * (A[i - 2][j - 1] +
                                     A[i - 2][j + 1] +
                                     A[i - 1][j - 2] + 
                                     A[i - 1][j + 2] + 
                                     A[i + 1][j - 2] + 
                                     A[i + 1][j + 2] + 
                                     A[i + 2][j - 1] +
                                     A[i + 2][j + 1] ) +
                                7 * (A[i - 2][j    ] +
                                     A[i    ][j - 2] +
                                     A[i    ][j + 2] +
                                     A[i + 2][j    ] ) +
                               16 * (A[i - 1][j - 1] +
                                     A[i - 1][j + 1] +
                                     A[i + 1][j - 1] +
                                     A[i + 1][j + 1] ) +
                               26 * (A[i - 1][j    ] +
                                     A[i    ][j - 1] +
                                     A[i    ][j + 1] +
                                     A[i + 1][j    ] ) +
                               41 *  A[i    ][j    ] ) / 273;
    }
}


int main(int argc, char *argv[])
{
    init_arrays();

    STENCILBENCH_IF_ENABLE_TIMER(StencilBench::Timer sb_timer);
    STENCILBENCH_IF_ENABLE_TIMER(sb_timer.start());

    gaussian_2d25pt_kernel();

    STENCILBENCH_IF_ENABLE_TIMER(sb_timer.stop());
    STENCILBENCH_IF_ENABLE_TIMER(sb_timer.print_s());

    STENCILBENCH_PREVENT_DCE(print_arrays());

    return 0;
}
