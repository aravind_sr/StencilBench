/*
 * hypterm.cpp
 * TODO UPDATE header and license
 *
 * Distributed under terms of the GNU LGPL3 license.
 */

#include <iostream>

#include "hypterm.hpp"
#include <stencilbench.hpp>

DTYPE flux_0[N][N][N];
DTYPE flux_1[N][N][N];
DTYPE flux_2[N][N][N];
DTYPE flux_3[N][N][N];
DTYPE flux_4[N][N][N];
DTYPE cons_1[N][N][N];
DTYPE cons_2[N][N][N];
DTYPE cons_3[N][N][N];
DTYPE cons_4[N][N][N];
DTYPE q_1[N][N][N];
DTYPE q_2[N][N][N];
DTYPE q_3[N][N][N];
DTYPE q_4[N][N][N];
DTYPE dxinv[N];

static void init_arrays()
{
    for (int i = 0; i < N; ++i)
    {
        for (int j = 0; j < N; ++j)
        {
            for (int k = 0; k < N; ++k)
            {
                cons_1[i][j][k] = (i + j + k + 0) / N;
                cons_2[i][j][k] = (i + j + k + 1) / N;
                cons_3[i][j][k] = (i + j + k + 2) / N;
                cons_3[i][j][k] = (i + j + k + 3) / N;
                q_1[i][j][k] = (i + j + k + 4) / N;
                q_2[i][j][k] = (i + j + k + 5) / N;
                q_3[i][j][k] = (i + j + k + 6) / N;
                q_3[i][j][k] = (i + j + k + 7) / N;
            }
        }
        dxinv[i] = (i * 2 + 1) / N;
    }
}

static void print_arrays()
{
    for (int i = 0; i < N; ++i)
    {
        for (int j = 0; j < N; ++j)
        {
            for (int k = 0; k < N; ++k)
            {
                std::cout << flux_1[i][j][k] << " " << flux_2[i][j][k] << " "  << flux_3[i][j][k] << " "  << flux_4[i][j][k] << " " ;
            }
            std::cout << '\n';
        }
        std::cout << "\n\n";
    }
}

static void hypterm_kernel()
{
    int i, j, k;
    for (k = 4; k < N - 4; k++)
    {
        for (j = 4; j < N - 4; j++)
        {
            for (i = 4; i < N - 4; i++)
            {
                flux_0[k][j][i] -= ((0.8f * (cons_1[k][j][i + 1] - cons_1[k][j][i - 1]) - 0.2f * (cons_1[k][j][i + 2] - cons_1[k][j][i - 2]) + 0.038f * (cons_1[k][j][i + 3] - cons_1[k][j][i - 3]) - 0.0035f * (cons_1[k][j][i + 4] - cons_1[k][j][i - 4])) * dxinv[0]);
                flux_0[k][j][i] -= (0.8f * (cons_2[k][j + 1][i] - cons_2[k][j - 1][i]) - 0.2f * (cons_2[k][j + 2][i] - cons_2[k][j - 2][i]) + 0.038f * (cons_2[k][j + 3][i] - cons_2[k][j - 3][i]) - 0.0035f * (cons_2[k][j + 4][i] - cons_2[k][j - 4][i])) * dxinv[1];
                flux_0[k][j][i] -= (0.8f * (cons_3[k + 1][j][i] - cons_3[k - 1][j][i]) - 0.2f * (cons_3[k + 2][j][i] - cons_3[k - 2][j][i]) + 0.038f * (cons_3[k + 3][j][i] - cons_3[k - 3][j][i]) - 0.0035f * (cons_3[k + 4][j][i] - cons_3[k - 4][j][i])) * dxinv[2];
            }
        }
    }
    for (k = 4; k < N - 4; k++)
    {
        for (j = 4; j < N - 4; j++)
        {
            for (i = 4; i < N - 4; i++)
            {
                flux_1[k][j][i] = -((0.8f * (cons_1[k][j][i + 1] * q_1[k][j][i + 1] - cons_1[k][j][i - 1] * q_1[k][j][i - 1] + (q_4[k][j][i + 1] - q_4[k][j][i - 1])) - 0.2f * (cons_1[k][j][i + 2] * q_1[k][j][i + 2] - cons_1[k][j][i - 2] * q_1[k][j][i - 2] + (q_4[k][j][i + 2] - q_4[k][j][i - 2])) + 0.038f * (cons_1[k][j][i + 3] * q_1[k][j][i + 3] - cons_1[k][j][i - 3] * q_1[k][j][i - 3] + (q_4[k][j][i + 3] - q_4[k][j][i - 3])) - 0.0035f * (cons_1[k][j][i + 4] * q_1[k][j][i + 4] - cons_1[k][j][i - 4] * q_1[k][j][i - 4] + (q_4[k][j][i + 4] - q_4[k][j][i - 4]))) * dxinv[0]);
                flux_1[k][j][i] -= (0.8f * (cons_1[k][j + 1][i] * q_2[k][j + 1][i] - cons_1[k][j - 1][i] * q_2[k][j - 1][i]) - 0.2f * (cons_1[k][j + 2][i] * q_2[k][j + 2][i] - cons_1[k][j - 2][i] * q_2[k][j - 2][i]) + 0.038f * (cons_1[k][j + 3][i] * q_2[k][j + 3][i] - cons_1[k][j - 3][i] * q_2[k][j - 3][i]) - 0.0035f * (cons_1[k][j + 4][i] * q_2[k][j + 4][i] - cons_1[k][j - 4][i] * q_2[k][j - 4][i])) * dxinv[1];
                flux_1[k][j][i] -= (0.8f * (cons_1[k + 1][j][i] * q_3[k + 1][j][i] - cons_1[k - 1][j][i] * q_3[k - 1][j][i]) - 0.2f * (cons_1[k + 2][j][i] * q_3[k + 2][j][i] - cons_1[k - 2][j][i] * q_3[k - 2][j][i]) + 0.038f * (cons_1[k + 3][j][i] * q_3[k + 3][j][i] - cons_1[k - 3][j][i] * q_3[k - 3][j][i]) - 0.0035f * (cons_1[k + 4][j][i] * q_3[k + 4][j][i] - cons_1[k - 4][j][i] * q_3[k - 4][j][i])) * dxinv[2];
            }
        }
    }
    for (k = 4; k < N - 4; k++)
    {
        for (j = 4; j < N - 4; j++)
        {
            for (i = 4; i < N - 4; i++)
            {
                flux_2[k][j][i] = -((0.8f * (cons_2[k][j][i + 1] * q_1[k][j][i + 1] - cons_2[k][j][i - 1] * q_1[k][j][i - 1]) - 0.2f * (cons_2[k][j][i + 2] * q_1[k][j][i + 2] - cons_2[k][j][i - 2] * q_1[k][j][i - 2]) + 0.038f * (cons_2[k][j][i + 3] * q_1[k][j][i + 3] - cons_2[k][j][i - 3] * q_1[k][j][i - 3]) - 0.0035f * (cons_2[k][j][i + 4] * q_1[k][j][i + 4] - cons_2[k][j][i - 4] * q_1[k][j][i - 4])) * dxinv[0]);
                flux_2[k][j][i] -= (0.8f * (cons_2[k][j + 1][i] * q_2[k][j + 1][i] - cons_2[k][j - 1][i] * q_2[k][j - 1][i] + (q_4[k][j + 1][i] - q_4[k][j - 1][i])) - 0.2f * (cons_2[k][j + 2][i] * q_2[k][j + 2][i] - cons_2[k][j - 2][i] * q_2[k][j - 2][i] + (q_4[k][j + 2][i] - q_4[k][j - 2][i])) + 0.038f * (cons_2[k][j + 3][i] * q_2[k][j + 3][i] - cons_2[k][j - 3][i] * q_2[k][j - 3][i] + (q_4[k][j + 3][i] - q_4[k][j - 3][i])) - 0.0035f * (cons_2[k][j + 4][i] * q_2[k][j + 4][i] - cons_2[k][j - 4][i] * q_2[k][j - 4][i] + (q_4[k][j + 4][i] - q_4[k][j - 4][i]))) * dxinv[1];
                flux_2[k][j][i] -= (0.8f * (cons_2[k + 1][j][i] * q_3[k + 1][j][i] - cons_2[k - 1][j][i] * q_3[k - 1][j][i]) - 0.2f * (cons_2[k + 2][j][i] * q_3[k + 2][j][i] - cons_2[k - 2][j][i] * q_3[k - 2][j][i]) + 0.038f * (cons_2[k + 3][j][i] * q_3[k + 3][j][i] - cons_2[k - 3][j][i] * q_3[k - 3][j][i]) - 0.0035f * (cons_2[k + 4][j][i] * q_3[k + 4][j][i] - cons_2[k - 4][j][i] * q_3[k - 4][j][i])) * dxinv[2];
            }
        }
    }

    for (k = 4; k < N - 4; k++)
    {
        for (j = 4; j < N - 4; j++)
        {
            for (i = 4; i < N - 4; i++)
            {
                flux_3[k][j][i] = -((0.8f * (cons_3[k][j][i + 1] * q_1[k][j][i + 1] - cons_3[k][j][i - 1] * q_1[k][j][i - 1]) - 0.2f * (cons_3[k][j][i + 2] * q_1[k][j][i + 2] - cons_3[k][j][i - 2] * q_1[k][j][i - 2]) + 0.038f * (cons_3[k][j][i + 3] * q_1[k][j][i + 3] - cons_3[k][j][i - 3] * q_1[k][j][i - 3]) - 0.0035f * (cons_3[k][j][i + 4] * q_1[k][j][i + 4] - cons_3[k][j][i - 4] * q_1[k][j][i - 4])) * dxinv[0]);
                flux_3[k][j][i] -= (0.8f * (cons_3[k][j + 1][i] * q_2[k][j + 1][i] - cons_3[k][j - 1][i] * q_2[k][j - 1][i]) - 0.2f * (cons_3[k][j + 2][i] * q_2[k][j + 2][i] - cons_3[k][j - 2][i] * q_2[k][j - 2][i]) + 0.038f * (cons_3[k][j + 3][i] * q_2[k][j + 3][i] - cons_3[k][j - 3][i] * q_2[k][j - 3][i]) - 0.0035f * (cons_3[k][j + 4][i] * q_2[k][j + 4][i] - cons_3[k][j - 4][i] * q_2[k][j - 4][i])) * dxinv[1];
                flux_3[k][j][i] -= (0.8f * (cons_3[k + 1][j][i] * q_3[k + 1][j][i] - cons_3[k - 1][j][i] * q_3[k - 1][j][i] + (q_4[k + 1][j][i] - q_4[k - 1][j][i])) - 0.2f * (cons_3[k + 2][j][i] * q_3[k + 2][j][i] - cons_3[k - 2][j][i] * q_3[k - 2][j][i] + (q_4[k + 2][j][i] - q_4[k - 2][j][i])) + 0.038f * (cons_3[k + 3][j][i] * q_3[k + 3][j][i] - cons_3[k - 3][j][i] * q_3[k - 3][j][i] + (q_4[k + 3][j][i] - q_4[k - 3][j][i])) - 0.0035f * (cons_3[k + 4][j][i] * q_3[k + 4][j][i] - cons_3[k - 4][j][i] * q_3[k - 4][j][i] + (q_4[k + 4][j][i] - q_4[k - 4][j][i]))) * dxinv[2];
            }
        }
    }
    for (k = 4; k < N - 4; k++)
    {
        for (j = 4; j < N - 4; j++)
        {
            for (i = 4; i < N - 4; i++)
            {
                flux_4[k][j][i] = -((0.8f * (cons_4[k][j][i + 1] * q_1[k][j][i + 1] - cons_4[k][j][i - 1] * q_1[k][j][i - 1] + (q_4[k][j][i + 1] * q_1[k][j][i + 1] - q_4[k][j][i - 1] * q_1[k][j][i - 1])) - 0.2f * (cons_4[k][j][i + 2] * q_1[k][j][i + 2] - cons_4[k][j][i - 2] * q_1[k][j][i - 2] + (q_4[k][j][i + 2] * q_1[k][j][i + 2] - q_4[k][j][i - 2] * q_1[k][j][i - 2])) + 0.038f * (cons_4[k][j][i + 3] * q_1[k][j][i + 3] - cons_4[k][j][i - 3] * q_1[k][j][i - 3] + (q_4[k][j][i + 3] * q_1[k][j][i + 3] - q_4[k][j][i - 3] * q_1[k][j][i - 3])) - 0.0035f * (cons_4[k][j][i + 4] * q_1[k][j][i + 4] - cons_4[k][j][i - 4] * q_1[k][j][i - 4] + (q_4[k][j][i + 4] * q_1[k][j][i + 4] - q_4[k][j][i - 4] * q_1[k][j][i - 4]))) * dxinv[0]);
                flux_4[k][j][i] -= (0.8f * (cons_4[k + 1][j][i] * q_3[k + 1][j][i] - cons_4[k - 1][j][i] * q_3[k - 1][j][i] + (q_4[k + 1][j][i] * q_3[k + 1][j][i] - q_4[k - 1][j][i] * q_3[k - 1][j][i])) - 0.2f * (cons_4[k + 2][j][i] * q_3[k + 2][j][i] - cons_4[k - 2][j][i] * q_3[k - 2][j][i] + (q_4[k + 2][j][i] * q_3[k + 2][j][i] - q_4[k - 2][j][i] * q_3[k - 2][j][i])) + 0.038f * (cons_4[k + 3][j][i] * q_3[k + 3][j][i] - cons_4[k - 3][j][i] * q_3[k - 3][j][i] + (q_4[k + 3][j][i] * q_3[k + 3][j][i] - q_4[k - 3][j][i] * q_3[k - 3][j][i])) - 0.0035f * (cons_4[k + 4][j][i] * q_3[k + 4][j][i] - cons_4[k - 4][j][i] * q_3[k - 4][j][i] + (q_4[k + 4][j][i] * q_3[k + 4][j][i] - q_4[k - 4][j][i] * q_3[k - 4][j][i]))) * dxinv[2];
                flux_4[k][j][i] -= (0.8f * (cons_4[k][j + 1][i] * q_2[k][j + 1][i] - cons_4[k][j - 1][i] * q_2[k][j - 1][i] + (q_4[k][j + 1][i] * q_2[k][j + 1][i] - q_4[k][j - 1][i] * q_2[k][j - 1][i])) - 0.2f * (cons_4[k][j + 2][i] * q_2[k][j + 2][i] - cons_4[k][j - 2][i] * q_2[k][j - 2][i] + (q_4[k][j + 2][i] * q_2[k][j + 2][i] - q_4[k][j - 2][i] * q_2[k][j - 2][i])) + 0.038f * (cons_4[k][j + 3][i] * q_2[k][j + 3][i] - cons_4[k][j - 3][i] * q_2[k][j - 3][i] + (q_4[k][j + 3][i] * q_2[k][j + 3][i] - q_4[k][j - 3][i] * q_2[k][j - 3][i])) - 0.0035f * (cons_4[k][j + 4][i] * q_2[k][j + 4][i] - cons_4[k][j - 4][i] * q_2[k][j - 4][i] + (q_4[k][j + 4][i] * q_2[k][j + 4][i] - q_4[k][j - 4][i] * q_2[k][j - 4][i]))) * dxinv[1];
            }
        }
    }

}


int main(int argc, char *argv[])
{
    init_arrays();

    STENCILBENCH_IF_ENABLE_TIMER(StencilBench::Timer sb_timer);
    STENCILBENCH_IF_ENABLE_TIMER(sb_timer.start());

    hypterm_kernel();

    STENCILBENCH_IF_ENABLE_TIMER(sb_timer.stop());
    STENCILBENCH_IF_ENABLE_TIMER(sb_timer.print_s());

    STENCILBENCH_PREVENT_DCE(print_arrays());

    return 0;
}
