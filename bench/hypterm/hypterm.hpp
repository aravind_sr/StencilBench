/*
 * hypterm.hpp
 * TODO UPDATE header and license
 *
 * Distributed under terms of the GNU LGPL3 license.
 */

#ifndef HYPTERM_HPP
#define HYPTERM_HPP

#define TINY   1
#define SMALL  2
#define MEDIUM 3
#define LARGE  4
#define HUGE   5

#ifndef DTYPE
#   define DTYPE double
#endif

#ifndef PROBLEM_SIZE
#   define PROBLEM_SIZE MEDIUM
#endif

#if PROBLEM_SIZE == TINY
#   define N      10
#elif PROBLEM_SIZE == SMALL
#   define N      30
#elif PROBLEM_SIZE == MEDIUM
#   define N      100
#elif PROBLEM_SIZE == LARGE
#   define N      150
#elif PROBLEM_SIZE == HUGE
#   define N      200
#endif

#endif /* !HYPTERM_HPP */

