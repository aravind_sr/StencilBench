/*
 * helmholtz.cpp
 * TODO UPDATE header and license
 *
 * Distributed under terms of the GNU LGPL3 license.
 */

#include <iostream>

#include "helmholtz.hpp"
#include <stencilbench.hpp>

DTYPE A  [N][N][N];
DTYPE B [N][N][N];
DTYPE alpha  [N][N][N];
DTYPE beta_i [N][N][N];
DTYPE beta_j [N][N][N];
DTYPE beta_k [N][N][N];
DTYPE rhs    [N][N][N];
DTYPE dinv   [N][N][N];

static void init_arrays()
{
    for (int i = 0; i < N; ++i)
    {
        for (int j = 0; j < N; ++j)
        {
            for (int k = 0; k < N; ++k)
            {
                A[i][j][k] = (i + j + k + 0) / N;
                alpha[i][j][k] = (i + j + k + 1) / N;
                beta_i[i][j][k] = (i + j + k + 2) / N;
                beta_j[i][j][k] = (i + j + k + 3) / N;
                beta_k[i][j][k] = (i + j + k + 4) / N;
                rhs[i][j][k] = (i + j + k + 5) / N;
                dinv[i][j][k] = (i + j + k + 6) / N;
            }
        }
    }
}

static void print_arrays()
{
    for (int i = 0; i < N; ++i)
    {
        for (int j = 0; j < N; ++j)
        {
            for (int k = 0; k < N; ++k)
            {
                std::cout << A[i][j][k] << " " << B[i][j][k] << " ";
            }
            std::cout << '\n';
        }
        std::cout << "\n\n";
    }
}

static void helmholtz_kernel()
{
    const DTYPE h2inv = 10 / 3;
    const DTYPE a = 11 / 3;
    const DTYPE b = 17 / 3;

    for (int t = 0; t < TSTEPS; ++t)
    {
        for (int k = 2; k < N - 2; ++k)
        {
            for (int j = 2; j < N - 2; ++j)
            {
                for (int i = 2; i < N - 2; ++i)
                {
                    DTYPE helmholtz = a * alpha[k][j][i] * A[k][j][i]
                                      - b * h2inv * (
                                          STENCIL_TWELFTH * (
                                              beta_i[k     ][j     ][i     ] * (15.0 * (A[k     ][j     ][i - 1 ] - A[k][j][i] ) - (A[k     ][j     ][i - 2 ] - A[k     ][j     ][i + 1 ] )) +
                                              beta_i[k     ][j     ][i + 1 ] * (15.0 * (A[k     ][j     ][i + 1 ] - A[k][j][i] ) - (A[k     ][j     ][i + 2 ] - A[k     ][j     ][i - 1 ] )) +
                                              beta_j[k     ][j     ][i     ] * (15.0 * (A[k     ][j - 1 ][i     ] - A[k][j][i] ) - (A[k     ][j - 2 ][i     ] - A[k     ][j + 1 ][i     ] )) +
                                              beta_j[k     ][j + 1 ][i     ] * (15.0 * (A[k     ][j + 1 ][i     ] - A[k][j][i] ) - (A[k     ][j + 2 ][i     ] - A[k     ][j - 1 ][i     ] )) +
                                              beta_k[k     ][j     ][i     ] * (15.0 * (A[k - 1 ][j     ][i     ] - A[k][j][i] ) - (A[k - 2 ][j     ][i     ] - A[k + 1 ][j     ][i     ] )) +
                                              beta_k[k + 1 ][j     ][i     ] * (15.0 * (A[k + 1 ][j     ][i     ] - A[k][j][i] ) - (A[k + 2 ][j     ][i     ] - A[k - 1 ][j     ][i     ] ))
                                          )
                                          + 0.25 * STENCIL_TWELFTH * (
                                              (beta_i[k     ][j + 1 ][i     ] - beta_i[k     ][j - 1 ][i     ] ) * (A[k     ][j + 1 ][i - 1 ] - A[k     ][j + 1 ][i     ] - A[k     ][j - 1 ][i - 1 ] + A[k     ][j - 1 ][i     ] ) +
                                              (beta_i[k + 1 ][j     ][i     ] - beta_i[k - 1 ][j     ][i     ] ) * (A[k + 1 ][j     ][i - 1 ] - A[k + 1 ][j     ][i     ] - A[k - 1 ][j     ][i - 1 ] + A[k - 1 ][j     ][i     ] ) +
                                              (beta_j[k     ][j     ][i + 1 ] - beta_j[k     ][j     ][i - 1 ] ) * (A[k     ][j - 1 ][i + 1 ] - A[k     ][j     ][i + 1 ] - A[k     ][j - 1 ][i - 1 ] + A[k     ][j     ][i - 1 ] ) +
                                              (beta_j[k + 1 ][j     ][i     ] - beta_j[k - 1 ][j     ][i     ] ) * (A[k + 1 ][j - 1 ][i     ] - A[k + 1 ][j     ][i     ] - A[k - 1 ][j - 1 ][i     ] + A[k - 1 ][j     ][i     ] ) +
                                              (beta_k[k     ][j     ][i + 1 ] - beta_k[k     ][j     ][i - 1 ] ) * (A[k - 1 ][j     ][i + 1 ] - A[k     ][j     ][i + 1 ] - A[k - 1 ][j     ][i - 1 ] + A[k     ][j     ][i - 1 ] ) +
                                              (beta_k[k     ][j + 1 ][i     ] - beta_k[k     ][j - 1 ][i     ] ) * (A[k - 1 ][j + 1 ][i     ] - A[k     ][j + 1 ][i     ] - A[k - 1 ][j - 1 ][i     ] + A[k     ][j - 1 ][i     ] ) +

                                              (beta_i[k     ][j + 1 ][i + 1 ] - beta_i[k     ][j - 1 ][i + 1 ] ) * (A[k     ][j + 1 ][i + 1 ] - A[k     ][j + 1 ][i     ] - A[k     ][j - 1 ][i + 1 ] + A[k     ][j - 1 ][i     ] ) +
                                              (beta_i[k + 1 ][j     ][i + 1 ] - beta_i[k - 1 ][j     ][i + 1 ] ) * (A[k + 1 ][j     ][i + 1 ] - A[k + 1 ][j     ][i     ] - A[k - 1 ][j     ][i + 1 ] + A[k - 1 ][j     ][i     ] ) +
                                              (beta_j[k     ][j + 1 ][i + 1 ] - beta_j[k     ][j + 1 ][i - 1 ] ) * (A[k     ][j + 1 ][i + 1 ] - A[k     ][j     ][i + 1 ] - A[k     ][j + 1 ][i - 1 ] + A[k     ][j     ][i - 1 ] ) +
                                              (beta_j[k + 1 ][j + 1 ][i     ] - beta_j[k - 1 ][j + 1 ][i     ] ) * (A[k + 1 ][j + 1 ][i     ] - A[k + 1 ][j     ][i     ] - A[k - 1 ][j + 1 ][i     ] + A[k - 1 ][j     ][i     ] ) +
                                              (beta_k[k + 1 ][j     ][i + 1 ] - beta_k[k + 1 ][j     ][i - 1 ] ) * (A[k + 1 ][j     ][i + 1 ] - A[k     ][j     ][i + 1 ] - A[k + 1 ][j     ][i - 1 ] + A[k     ][j     ][i - 1 ] ) +
                                              (beta_k[k + 1 ][j + 1 ][i     ] - beta_k[k + 1 ][j - 1 ][i     ] ) * (A[k + 1 ][j + 1 ][i     ] - A[k     ][j + 1 ][i     ] - A[k + 1 ][j - 1 ][i     ] + A[k     ][j - 1 ][i     ] )
                                          )
                                      );
                    B[k][j][i] = A[k][j][i] + WEIGHT * dinv[k][j][i] * (rhs[k][j][i] - helmholtz);
                }
            }
        }
	for (int k = 2; k < N - 2; ++k)
        {
            for (int j = 2; j < N - 2; ++j)
            {
                for (int i = 2; i < N - 2; ++i)
                {
                    DTYPE helmholtz = a * alpha[k][j][i] * B[k][j][i]
                                      - b * h2inv * (
                                          STENCIL_TWELFTH * (
                                              beta_i[k     ][j     ][i     ] * (15.0 * (B[k     ][j     ][i - 1 ] - B[k][j][i] ) - (B[k     ][j     ][i - 2 ] - B[k     ][j     ][i + 1 ] )) +
                                              beta_i[k     ][j     ][i + 1 ] * (15.0 * (B[k     ][j     ][i + 1 ] - B[k][j][i] ) - (B[k     ][j     ][i + 2 ] - B[k     ][j     ][i - 1 ] )) +
                                              beta_j[k     ][j     ][i     ] * (15.0 * (B[k     ][j - 1 ][i     ] - B[k][j][i] ) - (B[k     ][j - 2 ][i     ] - B[k     ][j + 1 ][i     ] )) +
                                              beta_j[k     ][j + 1 ][i     ] * (15.0 * (B[k     ][j + 1 ][i     ] - B[k][j][i] ) - (B[k     ][j + 2 ][i     ] - B[k     ][j - 1 ][i     ] )) +
                                              beta_k[k     ][j     ][i     ] * (15.0 * (B[k - 1 ][j     ][i     ] - B[k][j][i] ) - (B[k - 2 ][j     ][i     ] - B[k + 1 ][j     ][i     ] )) +
                                              beta_k[k + 1 ][j     ][i     ] * (15.0 * (B[k + 1 ][j     ][i     ] - B[k][j][i] ) - (B[k + 2 ][j     ][i     ] - B[k - 1 ][j     ][i     ] ))
                                          )
                                          + 0.25 * STENCIL_TWELFTH * (
                                              (beta_i[k     ][j + 1 ][i     ] - beta_i[k     ][j - 1 ][i     ] ) * (B[k     ][j + 1 ][i - 1 ] - B[k     ][j + 1 ][i     ] - B[k     ][j - 1 ][i - 1 ] + B[k     ][j - 1 ][i     ] ) +
                                              (beta_i[k + 1 ][j     ][i     ] - beta_i[k - 1 ][j     ][i     ] ) * (B[k + 1 ][j     ][i - 1 ] - B[k + 1 ][j     ][i     ] - B[k - 1 ][j     ][i - 1 ] + B[k - 1 ][j     ][i     ] ) +
                                              (beta_j[k     ][j     ][i + 1 ] - beta_j[k     ][j     ][i - 1 ] ) * (B[k     ][j - 1 ][i + 1 ] - B[k     ][j     ][i + 1 ] - B[k     ][j - 1 ][i - 1 ] + B[k     ][j     ][i - 1 ] ) +
                                              (beta_j[k + 1 ][j     ][i     ] - beta_j[k - 1 ][j     ][i     ] ) * (B[k + 1 ][j - 1 ][i     ] - B[k + 1 ][j     ][i     ] - B[k - 1 ][j - 1 ][i     ] + B[k - 1 ][j     ][i     ] ) +
                                              (beta_k[k     ][j     ][i + 1 ] - beta_k[k     ][j     ][i - 1 ] ) * (B[k - 1 ][j     ][i + 1 ] - B[k     ][j     ][i + 1 ] - B[k - 1 ][j     ][i - 1 ] + B[k     ][j     ][i - 1 ] ) +
                                              (beta_k[k     ][j + 1 ][i     ] - beta_k[k     ][j - 1 ][i     ] ) * (B[k - 1 ][j + 1 ][i     ] - B[k     ][j + 1 ][i     ] - B[k - 1 ][j - 1 ][i     ] + B[k     ][j - 1 ][i     ] ) +

                                              (beta_i[k     ][j + 1 ][i + 1 ] - beta_i[k     ][j - 1 ][i + 1 ] ) * (B[k     ][j + 1 ][i + 1 ] - B[k     ][j + 1 ][i     ] - B[k     ][j - 1 ][i + 1 ] + B[k     ][j - 1 ][i     ] ) +
                                              (beta_i[k + 1 ][j     ][i + 1 ] - beta_i[k - 1 ][j     ][i + 1 ] ) * (B[k + 1 ][j     ][i + 1 ] - B[k + 1 ][j     ][i     ] - B[k - 1 ][j     ][i + 1 ] + B[k - 1 ][j     ][i     ] ) +
                                              (beta_j[k     ][j + 1 ][i + 1 ] - beta_j[k     ][j + 1 ][i - 1 ] ) * (B[k     ][j + 1 ][i + 1 ] - B[k     ][j     ][i + 1 ] - B[k     ][j + 1 ][i - 1 ] + B[k     ][j     ][i - 1 ] ) +
                                              (beta_j[k + 1 ][j + 1 ][i     ] - beta_j[k - 1 ][j + 1 ][i     ] ) * (B[k + 1 ][j + 1 ][i     ] - B[k + 1 ][j     ][i     ] - B[k - 1 ][j + 1 ][i     ] + B[k - 1 ][j     ][i     ] ) +
                                              (beta_k[k + 1 ][j     ][i + 1 ] - beta_k[k + 1 ][j     ][i - 1 ] ) * (B[k + 1 ][j     ][i + 1 ] - B[k     ][j     ][i + 1 ] - B[k + 1 ][j     ][i - 1 ] + B[k     ][j     ][i - 1 ] ) +
                                              (beta_k[k + 1 ][j + 1 ][i     ] - beta_k[k + 1 ][j - 1 ][i     ] ) * (B[k + 1 ][j + 1 ][i     ] - B[k     ][j + 1 ][i     ] - B[k + 1 ][j - 1 ][i     ] + B[k     ][j - 1 ][i     ] )
                                          )
                                      );
                    A[k][j][i] = B[k][j][i] + WEIGHT * dinv[k][j][i] * (rhs[k][j][i] - helmholtz);
                }
            }
        }
    }
}


int main(int argc, char *argv[])
{
    init_arrays();

    STENCILBENCH_IF_ENABLE_TIMER(StencilBench::Timer sb_timer);
    STENCILBENCH_IF_ENABLE_TIMER(sb_timer.start());

    helmholtz_kernel();

    STENCILBENCH_IF_ENABLE_TIMER(sb_timer.stop());
    STENCILBENCH_IF_ENABLE_TIMER(sb_timer.print_s());

    STENCILBENCH_PREVENT_DCE(print_arrays());

    return 0;
}
