/*
 * denoise.cpp
 * TODO UPDATE header and license
 *
 * Distributed under terms of the GNU LGPL3 license.
 */

#include <iostream>
#include <cmath>
#undef HUGE
#include "denoise.hpp"
#include <stencilbench.hpp>

DTYPE u [N][N][N];
DTYPE u0[N][N][N];
DTYPE f [N][N][N];
DTYPE g [N][N][N];

static void init_arrays()
{
    for (int i = 0; i < N; ++i)
    {
        for (int j = 0; j < N; ++j)
        {
            for (int k = 0; k < N; ++k)
            {
                u0[i][j][k] = (i + j + k + 0) / N;
                f[i][j][k] = (i + j + k + 1) / N;
            }
        }
    }
}

static void print_arrays()
{
    for (int i = 0; i < N; ++i)
    {
        for (int j = 0; j < N; ++j)
        {
            for (int k = 0; k < N; ++k)
            {
                std::cout << u[i][j][k] << " " << g[i][j][k] << " ";
            }
            std::cout << '\n';
        }
        std::cout << "\n\n";
    }
}

static void denoise_kernel()
{
    DTYPE sigma2 = 0.05f * 0.05f;
    DTYPE gamma = 0.065f / sigma2;
    for (int i = 1; i < N - 1; i++)
    {
        for (int j = 1; j < N - 1; j++)
	{
            for (int k = 1; k < N - 1; k++)
            {
                g[i][j][k] = 1.0f / sqrt ( EPSILON + ( u0[i][j][k] - u0[i    ][j + 1][k    ]) *
                                           ( u0[i][j][k] - u0[i    ][j + 1][k    ]) + (u0[i][j][k] - u0[i    ][j - 1][k    ]) *
                                           ( u0[i][j][k] - u0[i    ][j - 1][k    ]) + (u0[i][j][k] - u0[i    ][j    ][k + 1]) *
                                           ( u0[i][j][k] - u0[i    ][j    ][k + 1]) + (u0[i][j][k] - u0[i    ][j    ][k - 1]) *
                                           ( u0[i][j][k] - u0[i    ][j    ][k - 1]) + (u0[i][j][k] - u0[i + 1][j    ][k    ]) *
                                           ( u0[i][j][k] - u0[i + 1][j    ][k    ]) + (u0[i][j][k] - u0[i - 1][j    ][k    ]) *
                                           ( u0[i][j][k] - u0[i - 1][j    ][k    ]));
            }
	}
    }

    for (int i = 1; i < N - 1; i++)
    {
        for (int j = 1; j < N - 1; j++)
	{
            for (int k = 1; k < N - 1; k++)
            {
                DTYPE r = u0[i][j][k] * f[i][j][k] / sigma2;
                r = (r * (2.38944f + r * (0.950037f + r))) / (4.65314f + r * (2.57541f + r * (1.48937f + r)));

                /* Update U */
                u[i][j][k] = ( u0[i][j][k] + 5.0f *
                               (u0[i][j + 1][k] *
                                g[i    ][j + 1][k    ] + u0[i    ][j - 1][k    ] *
                                g[i    ][j - 1][k    ] + u0[i    ][j    ][k + 1] *
                                g[i    ][j    ][k + 1] + u0[i    ][j    ][k - 1] *
                                g[i    ][j    ][k - 1] + u0[i + 1][j    ][k    ] *
                                g[i + 1][j    ][k    ] + u0[i - 1][j    ][k    ] *
                                g[i - 1][j    ][k    ] + gamma *
                                f[i][j][k] * r)) / (1.0f + 5.0f *
                                                    (g[i    ][j + 1][k    ] +
                                                     g[i    ][j - 1][k    ] +
                                                     g[i    ][j    ][k + 1] +
                                                     g[i    ][j    ][k - 1] +
                                                     g[i + 1][j    ][k    ] +
                                                     g[i - 1][j    ][k    ] + gamma));
            }
	}
    }
}


int main(int argc, char *argv[])
{
    init_arrays();

    STENCILBENCH_IF_ENABLE_TIMER(StencilBench::Timer sb_timer);
    STENCILBENCH_IF_ENABLE_TIMER(sb_timer.start());

    denoise_kernel();

    STENCILBENCH_IF_ENABLE_TIMER(sb_timer.stop());
    STENCILBENCH_IF_ENABLE_TIMER(sb_timer.print_s());

    STENCILBENCH_PREVENT_DCE(print_arrays());

    return 0;
}
