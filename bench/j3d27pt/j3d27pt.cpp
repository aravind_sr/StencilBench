/*
 * j3d27pt.cpp
 * TODO UPDATE header and license
 *
 * Distributed under terms of the GNU LGPL3 license.
 */

#include <iostream>

#include "j3d27pt.hpp"
#include <stencilbench.hpp>

DTYPE A[N][N][N];
DTYPE B[N][N][N];

static void init_arrays()
{
    for (int i = 0; i < N; ++i)
    {
        for (int j = 0; j < N; ++j)
        {
            for (int k = 0; k < N; ++k)
            {
                A[i][j][k] = (i + j + k) / N;
            }
        }
    }
}

static void print_arrays()
{
    for (int i = 0; i < N; ++i)
    {
        for (int j = 0; j < N; ++j)
        {
            for (int k = 0; k < N; ++k)
            {
                std::cout << A[i][j][k] << " " << B[i][j][k] << " ";
            }
            std::cout << '\n';
        }
        std::cout << "\n\n";
    }
}

static void j3d27pt_kernel()
{
    for (int t = 0; t < TSTEPS; ++t)
    {
        for (int i = 1; i < N - 1; i++)
        {
            for (int j = 1; j < N - 1; j++)
            {
                for (int k = 1; k < N - 1; k++)
                {
                    B[i][j][k] = STENCIL_COEF0 *  A[i    ][j    ][k    ] +
                                 STENCIL_COEF1 * (A[i - 1][j    ][k    ] +
                                                  A[i + 1][j    ][k    ] +
                                                  A[i    ][j - 1][k    ] +
                                                  A[i    ][j + 1][k    ] +
                                                  A[i    ][j    ][k + 1] +
                                                  A[i    ][j    ][k - 1] ) +
                                 STENCIL_COEF2 * (A[i - 1][j    ][k - 1] +
                                                  A[i - 1][j - 1][k    ] +
                                                  A[i - 1][j + 1][k    ] +
                                                  A[i - 1][j    ][k + 1] +
                                                  A[i    ][j - 1][k - 1] +
                                                  A[i    ][j + 1][k - 1] +
                                                  A[i    ][j - 1][k + 1] +
                                                  A[i    ][j + 1][k + 1] +
                                                  A[i + 1][j    ][k - 1] +
                                                  A[i + 1][j - 1][k    ] +
                                                  A[i + 1][j + 1][k    ] +
                                                  A[i + 1][j    ][k + 1] ) +
                                 STENCIL_COEF3 * (A[i - 1][j - 1][k - 1] +
                                                  A[i - 1][j + 1][k - 1] +
                                                  A[i - 1][j - 1][k + 1] +
                                                  A[i - 1][j + 1][k + 1] +
                                                  A[i + 1][j - 1][k - 1] +
                                                  A[i + 1][j + 1][k - 1] +
                                                  A[i + 1][j - 1][k + 1] +
                                                  A[i + 1][j + 1][k + 1] );
                }
            }
        }
        for (int i = 1; i < N - 1; i++)
        {
            for (int j = 1; j < N - 1; j++)
            {
                for (int k = 1; k < N - 1; k++)
                {
                    A[i][j][k] = STENCIL_COEF0 *  B[i    ][j    ][k    ] +
                                 STENCIL_COEF1 * (B[i - 1][j    ][k    ] +
                                                  B[i + 1][j    ][k    ] +
                                                  B[i    ][j - 1][k    ] +
                                                  B[i    ][j + 1][k    ] +
                                                  B[i    ][j    ][k + 1] +
                                                  B[i    ][j    ][k - 1] ) +
                                 STENCIL_COEF2 * (B[i - 1][j    ][k - 1] +
                                                  B[i - 1][j - 1][k    ] +
                                                  B[i - 1][j + 1][k    ] +
                                                  B[i - 1][j    ][k + 1] +
                                                  B[i    ][j - 1][k - 1] +
                                                  B[i    ][j + 1][k - 1] +
                                                  B[i    ][j - 1][k + 1] +
                                                  B[i    ][j + 1][k + 1] +
                                                  B[i + 1][j    ][k - 1] +
                                                  B[i + 1][j - 1][k    ] +
                                                  B[i + 1][j + 1][k    ] +
                                                  B[i + 1][j    ][k + 1] ) +
                                 STENCIL_COEF3 * (B[i - 1][j - 1][k - 1] +
                                                  B[i - 1][j + 1][k - 1] +
                                                  B[i - 1][j - 1][k + 1] +
                                                  B[i - 1][j + 1][k + 1] +
                                                  B[i + 1][j - 1][k - 1] +
                                                  B[i + 1][j + 1][k - 1] +
                                                  B[i + 1][j - 1][k + 1] +
                                                  B[i + 1][j + 1][k + 1] );
                }
            }
        }
    }
}


int main(int argc, char *argv[])
{
    init_arrays();

    STENCILBENCH_IF_ENABLE_TIMER(StencilBench::Timer sb_timer);
    STENCILBENCH_IF_ENABLE_TIMER(sb_timer.start());

    j3d27pt_kernel();

    STENCILBENCH_IF_ENABLE_TIMER(sb_timer.stop());
    STENCILBENCH_IF_ENABLE_TIMER(sb_timer.print_s());

    STENCILBENCH_PREVENT_DCE(print_arrays());

    return 0;
}
