/*
 * laplacian_13pt.cpp
 * TODO UPDATE header and license
 *
 * Distributed under terms of the GNU LGPL3 license.
 */

#include <iostream>

#include "laplacian_13pt.hpp"
#include <stencilbench.hpp>

DTYPE A[N][N][N];
DTYPE B[N][N][N];

static void init_arrays()
{
    for (int i = 0; i < N; ++i)
    {
        for (int j = 0; j < N; ++j)
        {
            for (int k = 0; k < N; ++k)
            {
                A[i][j][k] = (i + j + k) / N;
            }
        }
    }
}

static void print_arrays()
{
    for (int i = 0; i < N; ++i)
    {
        for (int j = 0; j < N; ++j)
        {
            for (int k = 0; k < N; ++k)
            {
                std::cout << A[i][j][k] << " " << B[i][j][k] << " ";
            }
            std::cout << '\n';
        }
        std::cout << "\n\n";
    }
}

static void laplacian_13pt_kernel()
{
    const DTYPE h2inv = 10 / 3;
    for (int t = 0; t < TSTEPS; ++t)
    {
        for (int k = 2; k < N - 2; ++k)
        {
            for (int j = 2; j < N - 2; ++j)
            {
                for (int i = 2; i < N - 2; ++i)
                {
                    B[k][j][i] = h2inv * STENCIL_TWELFTH * (
                                     - 1.00f * (A[k - 2][j][i] + A[k][j - 2][i] + A[k][j][i - 2] + A[k][j][i + 2] + A[k][j + 2][i] + A[k + 2][j][i])
                                     + 16.0f * (A[k - 1][j][i] + A[k][j - 1][i] + A[k][j][i - 1] + A[k][j][i + 1] + A[k][j + 1][i] + A[k + 1][j][i])
                                     - 90.0f * (A[k][j][i])
                                 );
                }
            }
        }
        for (int k = 2; k < N - 2; ++k)
        {
            for (int j = 2; j < N - 2; ++j)
            {
                for (int i = 2; i < N - 2; ++i)
                {
                    A[k][j][i] = h2inv * STENCIL_TWELFTH * (
                                     - 1.00f * (B[k - 2][j][i] + B[k][j - 2][i] + B[k][j][i - 2] + B[k][j][i + 2] + B[k][j + 2][i] + B[k + 2][j][i])
                                     + 16.0f * (B[k - 1][j][i] + B[k][j - 1][i] + B[k][j][i - 1] + B[k][j][i + 1] + B[k][j + 1][i] + B[k + 1][j][i])
                                     - 90.0f * (B[k][j][i])
                                 );
                }
            }
        }
    }
}


int main(int argc, char *argv[])
{
    init_arrays();

    STENCILBENCH_IF_ENABLE_TIMER(StencilBench::Timer sb_timer);
    STENCILBENCH_IF_ENABLE_TIMER(sb_timer.start());

    laplacian_13pt_kernel();

    STENCILBENCH_IF_ENABLE_TIMER(sb_timer.stop());
    STENCILBENCH_IF_ENABLE_TIMER(sb_timer.print_s());

    STENCILBENCH_PREVENT_DCE(print_arrays());

    return 0;
}
