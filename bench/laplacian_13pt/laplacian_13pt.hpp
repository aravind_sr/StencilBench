/*
 * j3d27pt.hpp
 * TODO UPDATE header and license
 *
 * Distributed under terms of the GNU LGPL3 license.
 */

#ifndef LAPLACIAN_13PT_HPP
#define LAPLACIAN_13PT_HPP

#define TINY   1
#define SMALL  2
#define MEDIUM 3
#define LARGE  4
#define HUGE   5

#ifndef DTYPE
#   define DTYPE double
#endif

#ifndef PROBLEM_SIZE
#   define PROBLEM_SIZE MEDIUM
#endif

#if PROBLEM_SIZE == TINY
#   define N      10
#   define TSTEPS 10
#elif PROBLEM_SIZE == SMALL
#   define N      30
#   define TSTEPS 20
#elif PROBLEM_SIZE == MEDIUM
#   define N      100
#   define TSTEPS 50
#elif PROBLEM_SIZE == LARGE
#   define N      300
#   define TSTEPS 250
#elif PROBLEM_SIZE == HUGE
#   define N      500
#   define TSTEPS 500
#endif

#define STENCIL_TWELFTH ( 0.0833333333333333333)

#endif /* !LAPLACIAN_13PT_HPP */
