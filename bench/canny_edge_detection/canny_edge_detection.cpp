/*
 * canny_edge_detection.cpp
 * TODO UPDATE header and license
 *
 * Distributed under terms of the GNU LGPL3 license.
 */

#include <iostream>

#include "canny_edge_detection.hpp"
#include <stencilbench.hpp>

DTYPE A[N][N];
DTYPE B[N][N];
DTYPE gaussian[N][N];

static void init_arrays()
{
    for (int i = 0; i < N; ++i)
    {
        for (int j = 0; j < N; ++j)
        {
            A[i][j] = (i + j + 0) / N;
            gaussian[i][j] = (i + j + 1) / N;
        }
    }
}

static void print_arrays()
{
    for (int i = 0; i < N; ++i)
    {
        for (int j = 0; j < N; ++j)
        {
            std::cout << A[i][j] << " " << B[i][j] << " ";
        }
        std::cout << "\n\n";
    }
}

static void canny_edge_detection_kernel()
{
    for (int t = 0; t < TSTEPS; ++t)
    {
        for( int i = 2 ; i < N - 2 ; i++ )
	{
            for( int j = 2 ; j < N - 2 ; j++ )
            {
                gaussian[i][j] =    (2 * ( A[i - 2][j - 2]+
                                           A[i + 2][j - 2]+
                                           A[i - 2][j + 2]+
                                           A[i + 2][j + 2]) +
                                     4 * ( A[i - 2][j - 1]+
                                           A[i - 2][j + 1]+
                                           A[i - 1][j - 2]+
                                           A[i - 1][j + 2]+
                                           A[i + 1][j - 2]+
                                           A[i + 1][j + 2]+
                                           A[i + 2][j - 1]+
                                           A[i + 2][j + 1]) +
                                     5 * ( A[i - 2][j    ]+
                                           A[i    ][j - 2]+
                                           A[i    ][j + 2]+
                                           A[i + 2][j    ]) +
                                     9 * ( A[i - 1][j - 1]+
                                           A[i - 1][j + 1]+
                                           A[i + 1][j - 1]+
                                           A[i + 1][j + 1]) +
                                     12 * (A[i - 1][j    ]+
                                           A[i    ][j - 1]+
                                           A[i    ][j + 1]+
                                           A[i + 1][j    ]) +
                                     15 *  A[i    ][j    ]) / 159;

            }
	}

        for( int i = 3 ; i < N - 3; i++ )
	{
            for( int j = 3 ; j < N - 3 ; j++ )
            {
                double gradientx = gaussian[i - 1][j + 1] - gaussian[i - 1][j - 1] + 2 * (gaussian[i][j + 1] - gaussian[i][j - 1]) + gaussian[i + 1][j + 1] - gaussian[i + 1][j - 1];
                double gradienty = gaussian[i + 1][j - 1] - gaussian[i - 1][j - 1] + 2 * (gaussian[i + 1][j] - gaussian[i - 1][j]) + gaussian[i + 1][j + 1] - gaussian[i - 1][j + 1];
                B[i][j] = gradientx + gradienty;
            }
	}
    }
}


int main(int argc, char *argv[])
{
    init_arrays();

    STENCILBENCH_IF_ENABLE_TIMER(StencilBench::Timer sb_timer);
    STENCILBENCH_IF_ENABLE_TIMER(sb_timer.start());

    canny_edge_detection_kernel();

    STENCILBENCH_IF_ENABLE_TIMER(sb_timer.stop());
    STENCILBENCH_IF_ENABLE_TIMER(sb_timer.print_s());

    STENCILBENCH_PREVENT_DCE(print_arrays());

    return 0;
}
