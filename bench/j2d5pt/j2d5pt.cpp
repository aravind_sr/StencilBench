/*
 * j2d5pt.cpp
 * TODO UPDATE header and license
 *
 * Distributed under terms of the GNU LGPL3 license.
 */

#include <iostream>

#include "j2d5pt.hpp"
#include <stencilbench.hpp>

DTYPE A[N][N];
DTYPE B[N][N];

static void init_arrays()
{
    for (int i = 0; i < N; ++i)
    {
        for (int j = 0; j < N; ++j)
        {
            A[i][j] = (i + j ) / N;
        }
    }
}

static void print_arrays()
{
    for (int i = 0; i < N; ++i)
    {
        for (int j = 0; j < N; ++j)
        {
            std::cout << A[i][j] << " " << B[i][j] << " ";
        }
        std::cout << "\n";
    }
}

static void j2d5pt_kernel()
{
    for (int t = 0; t < TSTEPS; ++t)
    {
        for( int i = 1; i < N - 1; i++ )
            for( int j = 1; j < N - 1; j++ )
                B[i][j] = ONEFIFTH * (A[i + 1][j] + A[i - 1][j] + A[i][j] + A[i][j - 1] + A[i][j + 1]);

        for( int i = 1; i < N - 1; i++ )
            for( int j = 1; j < N - 1; j++ )
                A[i][j] = ONEFIFTH * (B[i + 1][j] + B[i - 1][j] + B[i][j] + B[i][j - 1] + B[i][j + 1]);
    }
}


int main(int argc, char *argv[])
{
    init_arrays();

    STENCILBENCH_IF_ENABLE_TIMER(StencilBench::Timer sb_timer);
    STENCILBENCH_IF_ENABLE_TIMER(sb_timer.start());

    j2d5pt_kernel();

    STENCILBENCH_IF_ENABLE_TIMER(sb_timer.stop());
    STENCILBENCH_IF_ENABLE_TIMER(sb_timer.print_s());

    STENCILBENCH_PREVENT_DCE(print_arrays());

    return 0;
}
