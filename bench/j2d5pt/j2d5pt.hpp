/*
 * j2d5pt.hpp
 * TODO UPDATE header and license
 *
 * Distributed under terms of the GNU LGPL3 license.
 */

#ifndef J2D5PT_HPP
#define J2D5PT_HPP

#define TINY   1
#define SMALL  2
#define MEDIUM 3
#define LARGE  4
#define HUGE   5

#ifndef DTYPE
#   define DTYPE double
#endif

#ifndef PROBLEM_SIZE
#   define PROBLEM_SIZE MEDIUM
#endif

#if PROBLEM_SIZE == TINY
#   define N      10
#   define TSTEPS 10
#elif PROBLEM_SIZE == SMALL
#   define N      30
#   define TSTEPS 20
#elif PROBLEM_SIZE == MEDIUM
#   define N      500
#   define TSTEPS 100
#elif PROBLEM_SIZE == LARGE
#   define N      1000
#   define TSTEPS 300
#elif PROBLEM_SIZE == HUGE
#   define N      2000
#   define TSTEPS 500
#endif

#define ONEFIFTH (0.2f)

#endif /* !J2D5PT_HPP */
