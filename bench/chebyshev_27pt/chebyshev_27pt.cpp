/*
 * chebyshev_27pt.cpp
 * TODO UPDATE header and license
 *
 * Distributed under terms of the GNU LGPL3 license.
 */

#include <iostream>

#include "chebyshev_27pt.hpp"
#include <stencilbench.hpp>

DTYPE A[N][N][N];
DTYPE B[N][N][N];
DTYPE Ap[N][N][N];
DTYPE RHS[N][N][N];
DTYPE Dinv[N][N][N];


static void init_arrays()
{
    for (int i = 0; i < N; ++i)
    {
        for (int j = 0; j < N; ++j)
        {
            for (int k = 0; k < N; ++k)
            {
                A[i][j][k] = (i + j + k + 0) / N;
                Ap[i][j][k] = (i + j + k + 1) / N;
                RHS[i][j][k] = (i + j + k + 2) / N;
                Dinv[i][j][k] = (i + j + k + 3) / N;
            }
        }
    }
}

static void print_arrays()
{
    for (int i = 0; i < N; ++i)
    {
        for (int j = 0; j < N; ++j)
        {
            for (int k = 0; k < N; ++k)
            {
                std::cout << A[i][j][k] << " " << B[i][j][k] << " ";
            }
            std::cout << '\n';
        }
        std::cout << "\n\n";
    }
}

static void chebyshev_27pt_kernel()
{
    const DTYPE a = 10/3;
    const DTYPE b = 11/3;
    const DTYPE c1 = 17/3;
    const DTYPE c2 = 23/3;
    const DTYPE h2inv = 17/3;
    for (int t = 0; t < TSTEPS; ++t)
    {
        for (int k = 1; k < N - 1; k++)
        {
            for (int j = 1; j < N - 1; j++)
            {
                for (int i = 1; i < N - 1; i++)
                {
                    DTYPE MA =  a *                           A[k    ][j    ][i    ] -
                                b * h2inv * (STENCIL_COEF3 * (A[k - 1][j - 1][i - 1]+
                                                              A[k - 1][j - 1][i + 1]+
                                                              A[k - 1][j + 1][i - 1]+
                                                              A[k - 1][j + 1][i + 1]+
                                                              A[k + 1][j - 1][i - 1]+
                                                              A[k + 1][j - 1][i + 1]+
                                                              A[k + 1][j + 1][i - 1]+
                                                              A[k + 1][j + 1][i + 1]) +
                                             STENCIL_COEF2 * (A[k - 1][j - 1][i    ]+
                                                              A[k - 1][j + 1][i    ]+
                                                              A[k + 1][j - 1][i    ]+
                                                              A[k + 1][j + 1][i    ]+
                                                              A[k - 1][j    ][i - 1]+
                                                              A[k - 1][j    ][i + 1]+
                                                              A[k + 1][j    ][i - 1]+
                                                              A[k + 1][j    ][i + 1]+
                                                              A[k    ][j - 1][i - 1]+
                                                              A[k    ][j - 1][i + 1]+
                                                              A[k    ][j + 1][i - 1]+
                                                              A[k    ][j + 1][i + 1]) +
                                             STENCIL_COEF1 * (A[k - 1][j    ][i    ]+
                                                              A[k    ][j - 1][i    ]+
                                                              A[k    ][j    ][i - 1]+
                                                              A[k    ][j    ][i + 1]+
                                                              A[k    ][j + 1][i    ]+
                                                              A[k + 1][j    ][i    ]) -
                                             STENCIL_COEF0 *  A[k    ][j    ][i    ]);
                    B[k][j][i] = A[k][j][i] + c1 * (A[k][j][i] - Ap[k][j][i]) + c2 * Dinv[k][j][i] * (RHS[k][j][i] - MA);
                }
            }
        }
        for (int k = 1; k < N - 1; k++)
        {
            for (int j = 1; j < N - 1; j++)
            {
                for (int i = 1; i < N - 1; i++)
                {
                    DTYPE MA =  a *                           B[k    ][j    ][i    ] -
                                b * h2inv * (STENCIL_COEF3 * (B[k - 1][j - 1][i - 1]+
                                                              B[k - 1][j - 1][i + 1]+
                                                              B[k - 1][j + 1][i - 1]+
                                                              B[k - 1][j + 1][i + 1]+
                                                              B[k + 1][j - 1][i - 1]+
                                                              B[k + 1][j - 1][i + 1]+
                                                              B[k + 1][j + 1][i - 1]+
                                                              B[k + 1][j + 1][i + 1]) +
                                             STENCIL_COEF2 * (B[k - 1][j - 1][i    ]+
                                                              B[k - 1][j + 1][i    ]+
                                                              B[k + 1][j - 1][i    ]+
                                                              B[k + 1][j + 1][i    ]+
                                                              B[k - 1][j    ][i - 1]+
                                                              B[k - 1][j    ][i + 1]+
                                                              B[k + 1][j    ][i - 1]+
                                                              B[k + 1][j    ][i + 1]+
                                                              B[k    ][j - 1][i - 1]+
                                                              B[k    ][j - 1][i + 1]+
                                                              B[k    ][j + 1][i - 1]+
                                                              B[k    ][j + 1][i + 1]) +
                                             STENCIL_COEF1 * (B[k - 1][j    ][i    ]+
                                                              B[k    ][j - 1][i    ]+
                                                              B[k    ][j    ][i - 1]+
                                                              B[k    ][j    ][i + 1]+
                                                              B[k    ][j + 1][i    ]+
                                                              B[k + 1][j    ][i    ]) -
                                             STENCIL_COEF0 *  B[k    ][j    ][i    ]);
                    A[k][j][i] = A[k][j][i] + c1 * (A[k][j][i] - Ap[k][j][i]) + c2 * Dinv[k][j][i] * (RHS[k][j][i] - MA);
                }
            }
        }
    }
}


int main(int argc, char *argv[])
{
    init_arrays();

    STENCILBENCH_IF_ENABLE_TIMER(StencilBench::Timer sb_timer);
    STENCILBENCH_IF_ENABLE_TIMER(sb_timer.start());

    chebyshev_27pt_kernel();

    STENCILBENCH_IF_ENABLE_TIMER(sb_timer.stop());
    STENCILBENCH_IF_ENABLE_TIMER(sb_timer.print_s());

    STENCILBENCH_PREVENT_DCE(print_arrays());

    return 0;
}
