/*
 * fdtd3d.cpp
 * TODO UPDATE header and license
 *
 * Distributed under terms of the GNU LGPL3 license.
 */

#include <iostream>

#include "fdtd3d.hpp"
#include <stencilbench.hpp>

DTYPE Hx[N][N][N];
DTYPE Hy[N][N][N];
DTYPE Hz[N][N][N];
DTYPE Ex[N][N][N];
DTYPE Ey[N][N][N];
DTYPE Ez[N][N][N];

static void init_arrays()
{
    for (int i = 0; i < N; ++i)
    {
        for (int j = 0; j < N; ++j)
        {
            for (int k = 0; k < N; ++k)
            {
                Hx[i][j][k] = (i + j + k + 0) / N;
                Hy[i][j][k] = (i + j + k + 1) / N;
                Hz[i][j][k] = (i + j + k + 2) / N;
                Ex[i][j][k] = (i + j + k + 3) / N;
                Ey[i][j][k] = (i + j + k + 4) / N;
                Ez[i][j][k] = (i + j + k + 5) / N;
            }
        }
    }
}

static void print_arrays()
{
    for (int i = 0; i < N; ++i)
    {
        for (int j = 0; j < N; ++j)
        {
            for (int k = 0; k < N; ++k)
            {
                std::cout << Hx[i][j][k] << " " << Hy[i][j][k] << " " << Hy[i][j][k] << " ";
                std::cout << Ex[i][j][k] << " " << Ey[i][j][k] << " " << Ey[i][j][k] << " ";
            }
            std::cout << '\n';
        }
        std::cout << "\n\n";
    }
}

static void fdtd3d_kernel()
{
    for (int t = 0; t < TSTEPS; ++t)
    {
        for (int i = 1; i < N; i++)
        {
            for (int j = 1; j < N; j++)
            {
                for (int k = 1; k < N; k++)
                {
                    Ex[i][j][k] = STENCIL_COEF0 * Ex[i][j][k] + STENCIL_COEF1 * (Hz[i][j][k] - Hz[i    ][j - 1][k    ] ) + STENCIL_COEF2 * (Hy[i][j][k] - Hy[i    ][j    ][k - 1] );
                    Ey[i][j][k] = STENCIL_COEF0 * Ey[i][j][k] + STENCIL_COEF1 * (Hx[i][j][k] - Hx[i    ][j    ][k - 1] ) + STENCIL_COEF2 * (Hz[i][j][k] - Hz[i - 1][j    ][k    ] );
                    Ez[i][j][k] = STENCIL_COEF0 * Ez[i][j][k] + STENCIL_COEF1 * (Hy[i][j][k] - Hy[i - 1][j    ][k    ] ) + STENCIL_COEF2 * (Hz[i][j][k] - Hz[i    ][j - 1][k    ] );
                }
            }
        }
        for (int i = 0; i < N - 1; i++)
        {
            for (int j = 0; j < N - 1; j++)
            {
                for (int k = 0; k < N - 1; k++)
                {
                    Hx[i][j][k] = Hx[i][j][k] + STENCIL_COEF1 * (Ez[i    ][j + 1][k    ] - Ez[i][j][k] ) + STENCIL_COEF2 * (Ey[i    ][j    ][k + 1] - Ey[i][j][k] );
                    Hy[i][j][k] = Hy[i][j][k] + STENCIL_COEF1 * (Ex[i    ][j    ][k + 1] - Ex[i][j][k] ) + STENCIL_COEF2 * (Ez[i + 1][j    ][k    ] - Ez[i][j][k] );
                    Hz[i][j][k] = Hz[i][j][k] + STENCIL_COEF1 * (Ey[i + 1][j    ][k    ] - Ey[i][j][k] ) + STENCIL_COEF2 * (Ez[i    ][j + 1][k    ] - Ez[i][j][k] );
                }
            }
        }
    }
}


int main(int argc, char *argv[])
{
    init_arrays();

    STENCILBENCH_IF_ENABLE_TIMER(StencilBench::Timer sb_timer);
    STENCILBENCH_IF_ENABLE_TIMER(sb_timer.start());

    fdtd3d_kernel();

    STENCILBENCH_IF_ENABLE_TIMER(sb_timer.stop());
    STENCILBENCH_IF_ENABLE_TIMER(sb_timer.print_s());

    STENCILBENCH_PREVENT_DCE(print_arrays());

    return 0;
}
