/*
 * fdtd3d.hpp
 * TODO UPDATE header and license
 *
 * Distributed under terms of the GNU LGPL3 license.
 */

#ifndef FDTD3DPT_HPP
#define FDTD3DPT_HPP

#define TINY   1
#define SMALL  2
#define MEDIUM 3
#define LARGE  4
#define HUGE   5

#ifndef DTYPE
#   define DTYPE double
#endif

#ifndef PROBLEM_SIZE
#   define PROBLEM_SIZE MEDIUM
#endif

#if PROBLEM_SIZE == TINY
#   define N      10
#   define TSTEPS 10
#elif PROBLEM_SIZE == SMALL
#   define N      30
#   define TSTEPS 20
#elif PROBLEM_SIZE == MEDIUM
#   define N      100
#   define TSTEPS 50
#elif PROBLEM_SIZE == LARGE
#   define N      300
#   define TSTEPS 250
#elif PROBLEM_SIZE == HUGE
#   define N      300
#   define TSTEPS 300
#endif

#define STENCIL_COEF0 (0.0876)
#define STENCIL_COEF1 (0.0765) 
#define STENCIL_COEF2 (0.0654) 

#endif /* !FDTD3DPT_HPP */
