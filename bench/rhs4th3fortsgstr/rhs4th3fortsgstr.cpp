/*
 * rhs4th3fortsgstr.cpp
 * TODO UPDATE header and license
 *
 * Distributed under terms of the GNU LGPL3 license.
 */

#include <iostream>

#include "rhs4th3fortsgstr.hpp"
#include <stencilbench.hpp>

DTYPE uacc_0[N][N][N];
DTYPE uacc_1[N][N][N];
DTYPE uacc_2[N][N][N];
DTYPE u_0[N][N][N];      
DTYPE u_1[N][N][N];     
DTYPE u_2[N][N][N];    
DTYPE mu[N][N][N];   
DTYPE la[N][N][N];    
DTYPE strx[N];    
DTYPE stry[N];    
DTYPE strz[N];    

static void init_arrays()
{
    for (int i = 0; i < N; ++i)
    {
        for (int j = 0; j < N; ++j)
        {
            for (int k = 0; k < N; ++k)
            {
                uacc_0[i][j][k] = (i + j + k) / N;
                uacc_1[i][j][k] = (i + j + k + 1) / N;
                uacc_2[i][j][k] = (i + j + k + 2) / N;
                u_0[i][j][k] = (i + j + k + 3) / N;
                u_1[i][j][k] = (i + j + k + 4) / N;
                u_2[i][j][k] = (i + j + k + 5) / N;
                mu[i][j][k] = (i + j + k + 6) / N;
                la[i][j][k] = (i + j + k + 7) / N;
            }
        }

	strx[i] = (i * 2 + 1) / N;
	stry[i] = (i * 5 + 2) / N;
	strz[i] = (i * 8 + 3) / N;
    }
}

static void print_arrays()
{
    for (int i = 0; i < N; ++i)
    {
        for (int j = 0; j < N; ++j)
        {
            for (int k = 0; k < N; ++k)
            {
                std::cout << uacc_0[i][j][k] << " " << uacc_1[i][j][k] << " " << uacc_2[i][j][k] << " ";
            }
            std::cout << '\n';
        }
        std::cout << "\n\n";
    }
}

static void rhs4th3fortsgstr_kernel()
{
    DTYPE a1 = 1.2;
    DTYPE h = 3.7;
    DTYPE cof = 1e0 / ( h *  h);
    for (int k = 2; k < N - 2; k++)
    {
        for (int j = 2; j < N - 2; j++)
        {
            for (int i = 2; i < N - 2; i++)
            {
                DTYPE mux1 = mu[k][j][i - 1] * strx[i - 1] - 3e0 / 4 * (mu[k][j][i] * strx[i] + mu[k][j][i - 2] * strx[i - 2]);
                DTYPE mux2 = mu[k][j][i - 2] * strx[i - 2] + mu[k][j][i + 1] * strx[i + 1] + 3 * (mu[k][j][i] * strx[i] + mu[k][j][i - 1] * strx[i - 1]);
                DTYPE mux3 = mu[k][j][i - 1] * strx[i - 1] + mu[k][j][i + 2] * strx[i + 2] + 3 * (mu[k][j][i + 1] * strx[i + 1] + mu[k][j][i] * strx[i]);
                DTYPE mux4 = mu[k][j][i + 1] * strx[i + 1] - 3e0 / 4 * (mu[k][j][i] * strx[i] + mu[k][j][i + 2] * strx[i + 2]);
                DTYPE muy1 = mu[k][j - 1][i] * stry[j - 1] - 3e0 / 4 * (mu[k][j][i] * stry[j] + mu[k][j - 2][i] * stry[j - 2]);
                DTYPE muy2 = mu[k][j - 2][i] * stry[j - 2] + mu[k][j + 1][i] * stry[j + 1] + 3 * (mu[k][j][i] * stry[j] + mu[k][j - 1][i] * stry[j - 1]);
                DTYPE muy3 = mu[k][j - 1][i] * stry[j - 1] + mu[k][j + 2][i] * stry[j + 2] + 3 * (mu[k][j + 1][i] * stry[j + 1] + mu[k][j][i] * stry[j]);
                DTYPE muy4 = mu[k][j + 1][i] * stry[j + 1] - 3e0 / 4 * (mu[k][j][i] * stry[j] + mu[k][j + 2][i] * stry[j + 2]);
                DTYPE muz1 = mu[k - 1][j][i] * strz[k - 1] - 3e0 / 4 * (mu[k][j][i] * strz[k] + mu[k - 2][j][i] * strz[k - 2]);
                DTYPE muz2 = mu[k - 2][j][i] * strz[k - 2] + mu[k + 1][j][i] * strz[k + 1] + 3 * (mu[k][j][i] * strz[k] + mu[k - 1][j][i] * strz[k - 1]);
                DTYPE muz3 = mu[k - 1][j][i] * strz[k - 1] + mu[k + 2][j][i] * strz[k + 2] + 3 * (mu[k + 1][j][i] * strz[k + 1] + mu[k][j][i] * strz[k]);
                DTYPE muz4 = mu[k + 1][j][i] * strz[k + 1] - 3e0 / 4 * (mu[k][j][i] * strz[k] + mu[k + 2][j][i] * strz[k + 2]);

                DTYPE r1 = 1e0 / 6 * (strx[i] * ((2 * mux1 + la[k][j][i - 1] * strx[i - 1] - 3e0 / 4 * (la[k][j][i] * strx[i] + la[k][j][i - 2] * strx[i - 2])) * (u_0[k][j][i - 2] 
					- u_0[k][j][i]) + (2 * mux2 + la[k][j][i - 2] * strx[i - 2] + la[k][j][i + 1] * strx[i + 1] + 3 * (la[k][j][i] * strx[i] + la[k][j][i - 1] * strx[i - 1])) * (u_0[k][j][i - 1] 
					- u_0[k][j][i]) + (2 * mux3 + la[k][j][i - 1] * strx[i - 1] + la[k][j][i + 2] * strx[i + 2] + 3 * (la[k][j][i + 1] * strx[i + 1] + la[k][j][i] * strx[i])) * (u_0[k][j][i + 1] 
					- u_0[k][j][i]) + (2 * mux4 + la[k][j][i + 1] * strx[i + 1] - 3e0 / 4 * (la[k][j][i] * strx[i] + la[k][j][i + 2] * strx[i + 2])) * (u_0[k][j][i + 2] - u_0[k][j][i]))
			        	+ stry[j] * (muy1 * (u_0[k][j - 2][i] - u_0[k][j][i]) + muy2 * (u_0[k][j - 1][i] - u_0[k][j][i]) 
						   + muy3 * (u_0[k][j + 1][i] - u_0[k][j][i]) + muy4 * (u_0[k][j + 2][i] - u_0[k][j][i])) 
					+ strz[k] * (muz1 * (u_0[k - 2][j][i] - u_0[k][j][i]) + muz2 * (u_0[k - 1][j][i] - u_0[k][j][i])
					       	   + muz3 * (u_0[k + 1][j][i] - u_0[k][j][i]) + muz4 * (u_0[k + 2][j][i] - u_0[k][j][i])));

                DTYPE r2 = 1e0 / 6 * (strx[i] * (mux1 * (u_1[k][j][i - 2] - u_1[k][j][i]) + mux2 * (u_1[k][j][i - 1] - u_1[k][j][i]) + mux3 * (u_1[k][j][i + 1] - u_1[k][j][i]) + mux4 * (u_1[k][j][i + 2] - u_1[k][j][i])) + stry[j] * ((2 * muy1 + la[k][j - 1][i] * stry[j - 1] - 3e0 / 4 * (la[k][j][i] * stry[j] + la[k][j - 2][i] * stry[j - 2])) * (u_1[k][j - 2][i] - u_1[k][j][i]) + (2 * muy2 + la[k][j - 2][i] * stry[j - 2] + la[k][j + 1][i] * stry[j + 1] + 3 * (la[k][j][i] * stry[j] + la[k][j - 1][i] * stry[j - 1])) * (u_1[k][j - 1][i] - u_1[k][j][i]) + (2 * muy3 + la[k][j - 1][i] * stry[j - 1] + la[k][j + 2][i] * stry[j + 2] + 3 * (la[k][j + 1][i] * stry[j + 1] + la[k][j][i] * stry[j])) * (u_1[k][j + 1][i] - u_1[k][j][i]) + (2 * muy4 + la[k][j + 1][i] * stry[j + 1] - 3e0 / 4 * (la[k][j][i] * stry[j] + la[k][j + 2][i] * stry[j + 2])) * (u_1[k][j + 2][i] - u_1[k][j][i])) + strz[k] * (muz1 * (u_1[k - 2][j][i] - u_1[k][j][i]) + muz2 * (u_1[k - 1][j][i] - u_1[k][j][i]) + muz3 * (u_1[k + 1][j][i] - u_1[k][j][i]) + muz4 * (u_1[k + 2][j][i] - u_1[k][j][i])));

                DTYPE r3 = 1e0 / 6 * (strx[i] * (mux1 * (u_2[k][j][i - 2] - u_2[k][j][i]) + mux2 * (u_2[k][j][i - 1] - u_2[k][j][i]) + mux3 * (u_2[k][j][i + 1] - u_2[k][j][i]) + mux4 * (u_2[k][j][i + 2] - u_2[k][j][i])) + stry[j] * (muy1 * (u_2[k][j - 2][i] - u_2[k][j][i]) + muy2 * (u_2[k][j - 1][i] - u_2[k][j][i]) + muy3 * (u_2[k][j + 1][i] - u_2[k][j][i]) + muy4 * (u_2[k][j + 2][i] - u_2[k][j][i])) + strz[k] * ((2 * muz1 + la[k - 1][j][i] * strz[k - 1] - 3e0 / 4 * (la[k][j][i] * strz[k] + la[k - 2][j][i] * strz[k - 2])) * (u_2[k - 2][j][i] - u_2[k][j][i]) + (2 * muz2 + la[k - 2][j][i] * strz[k - 2] + la[k + 1][j][i] * strz[k + 1] + 3 * (la[k][j][i] * strz[k] + la[k - 1][j][i] * strz[k - 1])) * (u_2[k - 1][j][i] - u_2[k][j][i]) + (2 * muz3 + la[k - 1][j][i] * strz[k - 1] + la[k + 2][j][i] * strz[k + 2] + 3 * (la[k + 1][j][i] * strz[k + 1] + la[k][j][i] * strz[k])) * (u_2[k + 1][j][i] - u_2[k][j][i]) + (2 * muz4 + la[k + 1][j][i] * strz[k + 1] - 3e0 / 4 * (la[k][j][i] * strz[k] + la[k + 2][j][i] * strz[k + 2])) * (u_2[k + 2][j][i] - u_2[k][j][i])));

                r1 += strx[i] * stry[j] * (1e0 / 144) * (la[k][j][i - 2] * (u_1[k][j - 2][i - 2] - u_1[k][j + 2][i - 2] + 8 * (-u_1[k][j - 1][i - 2] + u_1[k][j + 1][i - 2])) - 8 * (la[k][j][i - 1] * (u_1[k][j - 2][i - 1] - u_1[k][j + 2][i - 1] + 8 * (-u_1[k][j - 1][i - 1] + u_1[k][j + 1][i - 1]))) + 8 * (la[k][j][i + 1] * (u_1[k][j - 2][i + 1] - u_1[k][j + 2][i + 1] + 8 * (-u_1[k][j - 1][i + 1] + u_1[k][j + 1][i + 1]))) - (la[k][j][i + 2] * (u_1[k][j - 2][i + 2] - u_1[k][j + 2][i + 2] + 8 * (-u_1[k][j - 1][i + 2] + u_1[k][j + 1][i + 2])))) + strx[i] * strz[k] * (1e0 / 144) * (la[k][j][i - 2] * (u_2[k - 2][j][i - 2] - u_2[k + 2][j][i - 2] + 8 * (-u_2[k - 1][j][i - 2] + u_2[k + 1][j][i - 2])) - 8 * (la[k][j][i - 1] * (u_2[k - 2][j][i - 1] - u_2[k + 2][j][i - 1] + 8 * (-u_2[k - 1][j][i - 1] + u_2[k + 1][j][i - 1]))) + 8 * (la[k][j][i + 1] * (u_2[k - 2][j][i + 1] - u_2[k + 2][j][i + 1] + 8 * (-u_2[k - 1][j][i + 1] + u_2[k + 1][j][i + 1]))) - (la[k][j][i + 2] * (u_2[k - 2][j][i + 2] - u_2[k + 2][j][i + 2] + 8 * (-u_2[k - 1][j][i + 2] + u_2[k + 1][j][i + 2])))) + strx[i] * stry[j] * (1e0 / 144) * (mu[k][j - 2][i] * (u_1[k][j - 2][i - 2] - u_1[k][j - 2][i + 2] + 8 * (-u_1[k][j - 2][i - 1] + u_1[k][j - 2][i + 1])) - 8 * (mu[k][j - 1][i] * (u_1[k][j - 1][i - 2] - u_1[k][j - 1][i + 2] + 8 * (-u_1[k][j - 1][i - 1] + u_1[k][j - 1][i + 1]))) + 8 * (mu[k][j + 1][i] * (u_1[k][j + 1][i - 2] - u_1[k][j + 1][i + 2] + 8 * (-u_1[k][j + 1][i - 1] + u_1[k][j + 1][i + 1]))) - (mu[k][j + 2][i] * (u_1[k][j + 2][i - 2] - u_1[k][j + 2][i + 2] + 8 * (-u_1[k][j + 2][i - 1] + u_1[k][j + 2][i + 1])))) + strx[i] * strz[k] * (1e0 / 144) * (mu[k - 2][j][i] * (u_2[k - 2][j][i - 2] - u_2[k - 2][j][i + 2] + 8 * (-u_2[k - 2][j][i - 1] + u_2[k - 2][j][i + 1])) - 8 * (mu[k - 1][j][i] * (u_2[k - 1][j][i - 2] - u_2[k - 1][j][i + 2] + 8 * (-u_2[k - 1][j][i - 1] + u_2[k - 1][j][i + 1]))) + 8 * (mu[k + 1][j][i] * (u_2[k + 1][j][i - 2] - u_2[k + 1][j][i + 2] + 8 * (-u_2[k + 1][j][i - 1] + u_2[k + 1][j][i + 1]))) - (mu[k + 2][j][i] * (u_2[k + 2][j][i - 2] - u_2[k + 2][j][i + 2] + 8 * (-u_2[k + 2][j][i - 1] + u_2[k + 2][j][i + 1]))));
                r2 += strx[i] * stry[j] * (1e0 / 144) * (mu[k][j][i - 2] * (u_0[k][j - 2][i - 2] - u_0[k][j + 2][i - 2] + 8 * (-u_0[k][j - 1][i - 2] + u_0[k][j + 1][i - 2])) - 8 * (mu[k][j][i - 1] * (u_0[k][j - 2][i - 1] - u_0[k][j + 2][i - 1] + 8 * (-u_0[k][j - 1][i - 1] + u_0[k][j + 1][i - 1]))) + 8 * (mu[k][j][i + 1] * (u_0[k][j - 2][i + 1] - u_0[k][j + 2][i + 1] + 8 * (-u_0[k][j - 1][i + 1] + u_0[k][j + 1][i + 1]))) - (mu[k][j][i + 2] * (u_0[k][j - 2][i + 2] - u_0[k][j + 2][i + 2] + 8 * (-u_0[k][j - 1][i + 2] + u_0[k][j + 1][i + 2])))) + strx[i] * stry[j] * (1e0 / 144) * (la[k][j - 2][i] * (u_0[k][j - 2][i - 2] - u_0[k][j - 2][i + 2] + 8 * (-u_0[k][j - 2][i - 1] + u_0[k][j - 2][i + 1])) - 8 * (la[k][j - 1][i] * (u_0[k][j - 1][i - 2] - u_0[k][j - 1][i + 2] + 8 * (-u_0[k][j - 1][i - 1] + u_0[k][j - 1][i + 1]))) + 8 * (la[k][j + 1][i] * (u_0[k][j + 1][i - 2] - u_0[k][j + 1][i + 2] + 8 * (-u_0[k][j + 1][i - 1] + u_0[k][j + 1][i + 1]))) - (la[k][j + 2][i] * (u_0[k][j + 2][i - 2] - u_0[k][j + 2][i + 2] + 8 * (-u_0[k][j + 2][i - 1] + u_0[k][j + 2][i + 1])))) + stry[j] * strz[k] * (1e0 / 144) * (la[k][j - 2][i] * (u_2[k - 2][j - 2][i] - u_2[k + 2][j - 2][i] + 8 * (-u_2[k - 1][j - 2][i] + u_2[k + 1][j - 2][i])) - 8 * (la[k][j - 1][i] * (u_2[k - 2][j - 1][i] - u_2[k + 2][j - 1][i] + 8 * (-u_2[k - 1][j - 1][i] + u_2[k + 1][j - 1][i]))) + 8 * (la[k][j + 1][i] * (u_2[k - 2][j + 1][i] - u_2[k + 2][j + 1][i] + 8 * (-u_2[k - 1][j + 1][i] + u_2[k + 1][j + 1][i]))) - (la[k][j + 2][i] * (u_2[k - 2][j + 2][i] - u_2[k + 2][j + 2][i] + 8 * (-u_2[k - 1][j + 2][i] + u_2[k + 1][j + 2][i])))) + stry[j] * strz[k] * (1e0 / 144) * (mu[k - 2][j][i] * (u_2[k - 2][j - 2][i] - u_2[k - 2][j + 2][i] + 8 * (-u_2[k - 2][j - 1][i] + u_2[k - 2][j + 1][i])) - 8 * (mu[k - 1][j][i] * (u_2[k - 1][j - 2][i] - u_2[k - 1][j + 2][i] + 8 * (-u_2[k - 1][j - 1][i] + u_2[k - 1][j + 1][i]))) + 8 * (mu[k + 1][j][i] * (u_2[k + 1][j - 2][i] - u_2[k + 1][j + 2][i] + 8 * (-u_2[k + 1][j - 1][i] + u_2[k + 1][j + 1][i]))) - (mu[k + 2][j][i] * (u_2[k + 2][j - 2][i] - u_2[k + 2][j + 2][i] + 8 * (-u_2[k + 2][j - 1][i] + u_2[k + 2][j + 1][i]))));
                r3 += strx[i] * strz[k] * (1e0 / 144) * (mu[k][j][i - 2] * (u_0[k - 2][j][i - 2] - u_0[k + 2][j][i - 2] + 8 * (-u_0[k - 1][j][i - 2] + u_0[k + 1][j][i - 2])) - 8 * (mu[k][j][i - 1] * (u_0[k - 2][j][i - 1] - u_0[k + 2][j][i - 1] + 8 * (-u_0[k - 1][j][i - 1] + u_0[k + 1][j][i - 1]))) + 8 * (mu[k][j][i + 1] * (u_0[k - 2][j][i + 1] - u_0[k + 2][j][i + 1] + 8 * (-u_0[k - 1][j][i + 1] + u_0[k + 1][j][i + 1]))) - (mu[k][j][i + 2] * (u_0[k - 2][j][i + 2] - u_0[k + 2][j][i + 2] + 8 * (-u_0[k - 1][j][i + 2] + u_0[k + 1][j][i + 2])))) + stry[j] * strz[k] * (1e0 / 144) * (mu[k][j - 2][i] * (u_1[k - 2][j - 2][i] - u_1[k + 2][j - 2][i] + 8 * (-u_1[k - 1][j - 2][i] + u_1[k + 1][j - 2][i])) - 8 * (mu[k][j - 1][i] * (u_1[k - 2][j - 1][i] - u_1[k + 2][j - 1][i] + 8 * (-u_1[k - 1][j - 1][i] + u_1[k + 1][j - 1][i]))) + 8 * (mu[k][j + 1][i] * (u_1[k - 2][j + 1][i] - u_1[k + 2][j + 1][i] + 8 * (-u_1[k - 1][j + 1][i] + u_1[k + 1][j + 1][i]))) - (mu[k][j + 2][i] * (u_1[k - 2][j + 2][i] - u_1[k + 2][j + 2][i] + 8 * (-u_1[k - 1][j + 2][i] + u_1[k + 1][j + 2][i])))) + strx[i] * strz[k] * (1e0 / 144) * (la[k - 2][j][i] * (u_0[k - 2][j][i - 2] - u_0[k - 2][j][i + 2] + 8 * (-u_0[k - 2][j][i - 1] + u_0[k - 2][j][i + 1])) - 8 * (la[k - 1][j][i] * (u_0[k - 1][j][i - 2] - u_0[k - 1][j][i + 2] + 8 * (-u_0[k - 1][j][i - 1] + u_0[k - 1][j][i + 1]))) + 8 * (la[k + 1][j][i] * (u_0[k + 1][j][i - 2] - u_0[k + 1][j][i + 2] + 8 * (-u_0[k + 1][j][i - 1] + u_0[k + 1][j][i + 1]))) - (la[k + 2][j][i] * (u_0[k + 2][j][i - 2] - u_0[k + 2][j][i + 2] + 8 * (-u_0[k + 2][j][i - 1] + u_0[k + 2][j][i + 1])))) + stry[j] * strz[k] * (1e0 / 144) * (la[k - 2][j][i] * (u_1[k - 2][j - 2][i] - u_1[k - 2][j + 2][i] + 8 * (-u_1[k - 2][j - 1][i] + u_1[k - 2][j + 1][i])) - 8 * (la[k - 1][j][i] * (u_1[k - 1][j - 2][i] - u_1[k - 1][j + 2][i] + 8 * (-u_1[k - 1][j - 1][i] + u_1[k - 1][j + 1][i]))) + 8 * (la[k + 1][j][i] * (u_1[k + 1][j - 2][i] - u_1[k + 1][j + 2][i] + 8 * (-u_1[k + 1][j - 1][i] + u_1[k + 1][j + 1][i]))) - (la[k + 2][j][i] * (u_1[k + 2][j - 2][i] - u_1[k + 2][j + 2][i] + 8 * (-u_1[k + 2][j - 1][i] + u_1[k + 2][j + 1][i]))));

                uacc_0[k][j][i] = a1 * uacc_0[k][j][i] + cof * r1;
                uacc_1[k][j][i] = a1 * uacc_1[k][j][i] + cof * r2;
                uacc_2[k][j][i] = a1 * uacc_2[k][j][i] + cof * r3;
            }
        }
    }
}


int main(int argc, char *argv[])
{
    init_arrays();

    STENCILBENCH_IF_ENABLE_TIMER(StencilBench::Timer sb_timer);
    STENCILBENCH_IF_ENABLE_TIMER(sb_timer.start());

    rhs4th3fortsgstr_kernel();

    STENCILBENCH_IF_ENABLE_TIMER(sb_timer.stop());
    STENCILBENCH_IF_ENABLE_TIMER(sb_timer.print_s());

    STENCILBENCH_PREVENT_DCE(print_arrays());

    return 0;
}
