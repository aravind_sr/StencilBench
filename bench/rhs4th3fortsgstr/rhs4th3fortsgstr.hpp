/*
 * rhs4th3fortsgstr.hpp
 * TODO UPDATE header and license
 *
 * Distributed under terms of the GNU LGPL3 license.
 */

#ifndef RHS4TH3FORTSGSTR_HPP
#define RHS4TH3FORTSGSTR_HPP

#define TINY   1
#define SMALL  2
#define MEDIUM 3
#define LARGE  4
#define HUGE   5

#ifndef DTYPE
#   define DTYPE double
#endif

#ifndef PROBLEM_SIZE
#   define PROBLEM_SIZE MEDIUM
#endif

#if PROBLEM_SIZE == TINY
#   define N      10
#elif PROBLEM_SIZE == SMALL
#   define N      30
#elif PROBLEM_SIZE == MEDIUM
#   define N      100
#elif PROBLEM_SIZE == LARGE
#   define N      200
#elif PROBLEM_SIZE == HUGE
#   define N      300
#endif

#define tf (3d0/4)
#define i6 (1d0/6)
#define i144 (1d0/144)
#define i12 (1d0/12)
#define d4a (2d0/3)
#define d4b (-(1d0/12))


#endif /* !RHS4TH3FORTSGSTR_HPP */

