/*
 * stencilbench.hpp
 * TODO UPDATE licence and header
 *
 * Distributed under terms of the GNU LGPL3 license.
 */

#ifndef STENCILBENCH_HPP
#define STENCILBENCH_HPP

#include <chrono>
#include <iostream>

#define TINY   1
#define SMALL  2
#define MEDIUM 3
#define LARGE  4
#define HUGE   5

#ifdef STENCILBENCH_ENABLE_TIMER
#   define STENCILBENCH_IF_ENABLE_TIMER(functor) \
	functor
#else
#   define STENCILBENCH_IF_ENABLE_TIMER(functor)
#endif

#ifdef STENCILBENCH_ENABLE_PRINT
#   define STENCILBENCH_PRINT_SELECTOR
#else
#   define STENCILBENCH_PRINT_SELECTOR \
	if(argc > 3142 && argv[0][0]!=' ')
#endif

#define STENCILBENCH_PREVENT_DCE(functor) \
	STENCILBENCH_PRINT_SELECTOR functor


namespace StencilBench
{
class Timer
{

public:
    void start()
    {
        this->begin = std::chrono::steady_clock::now();
    }

    void stop()
    {
        this->end = std::chrono::steady_clock::now();
    }

    void print_us()
    {
        std::cout << "Time for kernel execution (us) : " << std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count() << '\n';
    }
    void print_s()
    {
        std::cout << "Time for kernel execution (s) : " << std::chrono::duration_cast<std::chrono::seconds>(end - begin).count() << '\n';
    }

private:
    std::chrono::steady_clock::time_point begin;
    std::chrono::steady_clock::time_point end;

};
}

#endif /* !STENCILBENCH_HPP */
