#! /usr/bin/env python3
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# TODO update License and header
# Copyright © 2016
#
# Distributed under terms of the GNU LGPL3 license.

"""
 Stencil Bench
"""

from optparse import OptionParser
import sys
import os

g_bench_list = ['j3d27pt', 'j3d7pt', 'j2d5pt', 'rhs4th3fortsgstr', 'poission_19pt', 'laplacian_13pt', 'hypterm',
                'helmholtz', 'heat3d', 'gaussian_2d25pt', 'fdtd3d', 'denoise', 'chebyshev_27pt', 'canny_edge_detection']

def check_args(opts):
    if opts.command not in ['compile', 'run', 'clean']:
        print("Unknown command. Use -h, --help to see usage")
        sys.exit(-1)
    if opts.target not in ['CPU', 'GPU']:
        print("Unknown target. Use -h, --help to see usage")
        sys.exit(-1)
    if opts.bench not in g_bench_list:
        if opts.bench != "all":
            print("Unknown benchmark. Use -h, --help to see usage")
            sys.exit(-1)
    if opts.problem_size not in ['tiny', 'small', 'medium', 'large', 'huge']:
        print("Unknown problem size. Use -h, --help to see usage")
        sys.exit(-1)

    if opts.command != "compile":
        if opts.enable_timer:
            print("WARNING: Enable time is only valid for target = compile")
        if opts.enable_print:
            print("WARNING: Enable print is only valid for target = compile")


def process_cpu(opts):
    flags = " CXX_FLAGS+=' -DPROBLEM_SIZE=" + opts.problem_size.upper()
    if opts.enable_timer:
        flags += " -DSTENCILBENCH_ENABLE_TIMER"
    if opts.enable_print:
        flags += " -DSTENCILBENCH_ENABLE_PRINT"
    flags += "'"

    bench_list = [opts.bench]
    if opts.bench == 'all':
        bench_list = g_bench_list

    if opts.command == 'compile':
        for b in bench_list:
            os.system("make -C ./bench/" + b + "/ clean")
            os.system("make -C ./bench/" + b + "/" + flags)
    elif opts.command == 'clean':
        for b in bench_list:
            os.system("make -C ./bench/" + b + "/ clean")
    elif opts.command == 'run':
        for b in bench_list:
            os.system("./bench/" + b + "/" + b)


def process_gpu(opts):
    print("GPU target is not yet supported")
    sys.exit(-1)


def main(argv):
    parser = OptionParser()
    parser.add_option("-c", "--command", dest="command",
                      help="valid commands are compile, run, clean. Default is compile",
                      default="compile", action="store")
    parser.add_option("-t", "--target", dest="target", help="valid targets are CPU, GPU. Default is CPU", default="CPU",
                      action="store")
    parser.add_option("-b", "--bench", dest="bench", help="The name of benchmark. Default is all", default="all",
                      action="store")
    parser.add_option("-s", "--problem_size", dest="problem_size",
                      help="valid problem sizes are tiny, small, medium, large, huge. Default is medium",
                      default="medium",
                      action="store")
    parser.add_option("-T", "--enable_timer", dest="enable_timer", help="Enable time measurements. Default is False",
                      default=False, action="store_true")
    parser.add_option("-P", "--enable_print", dest="enable_print", help="Enable Printing results. Default is False",
                      default=False, action="store_true")

    (opts, args) = parser.parse_args()
    check_args(opts)

    if opts.target == 'CPU':
        process_cpu(opts)
    else:
        process_gpu(opts)

if __name__ == "__main__":
    main(sys.argv)
