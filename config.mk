#
# TODO Update License and 
#

ROOT_DIR:=$(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))

export CXX=g++
export override CXX_FLAGS+=-O3 -Wall -I$(ROOT_DIR)/common/ -std=c++11
export LD_FLAGS+=-lm
export DEPS=./common/stencilbench.hpp

# vim:ft=make
#
